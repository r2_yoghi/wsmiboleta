import { FolioController } from '../controller/FolioController';
import { checkJwt } from '../middlewares/jwt';
import { Router } from 'express';

const router = Router();

// Obtener folio para firmar
router.get('/folioget:cliente', FolioController.getFolio);

router.patch('/folioprep:cliente', FolioController.prepFolio);

router.post('/folioadd',FolioController.newFolio);

export default router;