import { checkJwt } from './../middlewares/jwt';
import { BoletaController } from './../controller/BoletaController';
import { Router } from 'express';

const router = Router();

// Lista de boletas
//router.get('/boletaslist',[checkJwt], BoletaController.getBoletaAll);


// Obtener detalle de boleta
//router.get('/boletadetalle:id',[checkJwt], BoletaController.getBoletaById);

//Boletas por rango de fecha
router.get('/boletasDate/:idUsuario',[checkJwt], BoletaController.getBoletaByDate);


// Crear nueva boleta
router.post('/nuevaboleta',[checkJwt], BoletaController.newBoleta);


// Anular boleta
router.patch('/editboleta/:id',[checkJwt], BoletaController.editBoleta);



export default router;