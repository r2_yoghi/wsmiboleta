import { checkJwt } from './../middlewares/jwt';
import { UserController } from './../controller/UserController';
import { Router } from 'express';

const router = Router();

// Get all users
router.get('/userlist',[checkJwt], UserController.getAll);


// Get one user
router.get('/user:id',[checkJwt], UserController.getById);



// Create new user
router.post('/useradd', UserController.newUser);


// Edit user
router.patch('/useredit/:id',[checkJwt], UserController.editUser);

// Delete
router.delete('/user:id',[checkJwt], UserController.deleteUser);


export default router;