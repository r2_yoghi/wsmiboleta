import {Router} from 'express';

import auth from './auth';
import user from './user';
import boleta from './boleta'
import folio from './folio';

const routes = Router();

routes.use('/auth', auth);
routes.use('/users', user);
routes.use('/boletas', boleta);
routes.use('/folios', folio);

export default routes;