import { Region } from './Region';
import {Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn} from "typeorm";
import {MinLength, IsNotEmpty, IsEmail} from 'class-validator';
import * as bcrypt from 'bcryptjs';

    @Entity()

export class Provincia {
    @PrimaryGeneratedColumn()
    idProvincia: number;

    @Column()
    provinciaNombre: string;

   @ManyToOne(type => Region)
    @JoinColumn()
    region: Region

}