import { Cliente } from './Cliente';
import { Boleta } from './Boleta';
import {Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, OneToMany, JoinColumn,ManyToOne} from "typeorm";
import {MinLength,MaxLength, IsNotEmpty, IsEmail} from 'class-validator';
import * as bcrypt from 'bcryptjs';

enum UsuarioEstado {ACTIVO = "ACTIVO", INACTIVO = "INACTIVO", ELIMINADO = "ELIMINADO"}

@Entity()
@Unique(['username'])
export class Usuario {

    @PrimaryGeneratedColumn()
    usuarioId: number;

    @Column()
    @MinLength(6)
    username: string;

    @Column()
    @MinLength(3)
    nombre: string;

    @Column()
    @MinLength(3)
    a_paterno: string;

    @Column()
    @MinLength(3)
    a_materno: string;

    @Column()
    @IsEmail()
    email: string;

    @Column()
    @MaxLength(13)
    @MinLength(10)
    rut: string;

    @Column()
    @MinLength(6)
    password: string;

    @ManyToOne(type => Cliente)
    @JoinColumn()
    cliente: Cliente

    @Column("enum", { enum: UsuarioEstado, default: "ACTIVO" })
    usuariEstado: UsuarioEstado;




    hashPassword():void {
        const salt = bcrypt.genSaltSync(10);
        this.password = bcrypt.hashSync(this.password, salt)
    }

    checkPassword(password: string):boolean {
        return bcrypt.compareSync(password, this.password);
    }
}
