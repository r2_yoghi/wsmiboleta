import { Provincia } from './Provincia';
import {Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn} from "typeorm";
import {MinLength, IsNotEmpty, IsEmail} from 'class-validator';
import * as bcrypt from 'bcryptjs';

    @Entity()
    
export class Comuna {
    @PrimaryGeneratedColumn()
    comunaId: number;

    @Column()
    comunaNombre: string;

    @ManyToOne(type => Provincia)
    @JoinColumn()
    provincia: Provincia

}