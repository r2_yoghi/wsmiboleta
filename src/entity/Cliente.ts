import { Comuna } from './Comuna';
import { Giro } from './Giro';
import {Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn} from "typeorm";
import {MinLength, IsNotEmpty, IsEmail} from 'class-validator';
import * as bcrypt from 'bcryptjs';

@Entity()
@Unique(['rut'])
export class Cliente {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    razon: string

    @ManyToOne(type => Giro)
    @JoinColumn()
    giro: Giro

    @Column()
    rut: string

    @Column()
    representante: string

    @Column()
    telefono: string

    @Column()
    email: string

    @Column()
    direccion: string

    @ManyToOne(type => Comuna)
    @JoinColumn()
    comuna: Comuna

}