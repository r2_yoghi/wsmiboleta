import { Cliente } from './Cliente';
import {Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, Timestamp, Double, JoinColumn, ManyToOne} from "typeorm";
import {MinLength, IsNotEmpty, IsEmail} from 'class-validator';
import * as bcrypt from 'bcryptjs';
import { type } from 'os';


enum FolioEstado {DISPONIBLE = "DISPONIBLE", PREPARADO = "PREPARADO", FIRMADO = "FIRMADO"}

@Entity()
@Unique(['folio'])
export class Folio {
@PrimaryGeneratedColumn()
folioId: number;

@Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
fechaCarga: Date

@Column()
folio: string;

@Column("enum", { enum: FolioEstado, default: "DISPONIBLE" })
folioEstado: FolioEstado;

@ManyToOne(type => Cliente)
@JoinColumn()
cliente: Cliente;
}


