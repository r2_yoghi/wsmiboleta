import { Folio } from './Folio';
import {Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, Timestamp, Double, ManyToOne, JoinColumn,OneToOne} from "typeorm";
import {MinLength, IsNotEmpty, IsEmail} from 'class-validator';
import * as bcrypt from 'bcryptjs';
import {Usuario} from './User';


enum BoletaEstado {VALIDA = "VALIDA", NULA = "NULA"}

@Entity()

export class Boleta {
    @PrimaryGeneratedColumn()
    boletaId: number;
    
    @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
    fecha: Date

    @Column()
    monto: number

    @Column("decimal", { precision: 7, scale: 2 })
    iva: number

    
    @Column("enum", { enum: BoletaEstado, default: "VALIDA" })
    boletaEstado: BoletaEstado;

    @ManyToOne(type => Usuario)
    @JoinColumn()
    usuario: Usuario

    @OneToOne(type => Folio)
    @JoinColumn()
    folio:Folio;



}