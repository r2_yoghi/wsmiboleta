import {Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, Timestamp, Double, ManyToOne, JoinColumn} from "typeorm";
import {MinLength, IsNotEmpty, IsEmail} from 'class-validator';
import * as bcrypt from 'bcryptjs';


@Entity()
@Unique(['codigo'])
export class Giro {
    @PrimaryGeneratedColumn()
    idGiro: number;

    @Column()
    codigo: string

    @Column()
    actividad: string

}