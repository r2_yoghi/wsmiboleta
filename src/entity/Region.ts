import {Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn} from "typeorm";
import {MinLength, IsNotEmpty, IsEmail} from 'class-validator';
import * as bcrypt from 'bcryptjs';

    @Entity()
    
export class Region {
    @PrimaryGeneratedColumn()
    RegionId: number;

    @Column()
    regionNombre: string;

}