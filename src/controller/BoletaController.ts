import { Folio } from './../entity/Folio';
import { FolioController } from './FolioController';
import { Boleta } from './../entity/Boleta';
import { getRepository } from 'typeorm';
import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import config from '../config/config';
import {validate} from 'class-validator';

import { Usuario } from './../entity/User';
import { Cliente } from './../entity/Cliente';


export class BoletaController {
    // static getBoletaAll = async (req:Request, res:Response) => {
    //     const boletaRepository = getRepository(Boleta);
        
    //     try {
    //         const boletas = await boletaRepository.find();
    //         if (boletas.length > 0) {
    //             res.send(boletas);
    //         } else {
    //             res.status(404).json({
    //                 message: 'No result'
    //             })
    //         }
    //     } catch (e) {
    //         return res.status(404).json({
    //             message: 'Unable to get rows'
    //         })
    //     }
    // }


    // static getBoletaById = async (req:Request, res:Response) => {
    //     const {id} = req.params;
    //     const boletaRepository = getRepository(Boleta);

    //     try {
    //         const boleta = await boletaRepository.findOneOrFail(id);
    //         res.send(boleta);
    //     } catch (e) {
    //         return res.status(404).json({
    //             message: 'Not result'
    //         })
    //     }
        

    // }

    static getBoletaByDate = async (req:Request, res:Response) => {
        const {idUsuario} = req.params;
        const {fecIni, fecFin} = req.query;
        try {
            const subQuery = await getRepository(Usuario)
            .createQueryBuilder('usuario')
            .select("usuario.cliente","cliente")
            .where("usuario.usuarioId = :idUsuario",{idUsuario})
            .getRawOne();
            
            const boleta = await getRepository(Boleta)
        
            .createQueryBuilder('boleta')
            .select("u.username","username")
            .addSelect('f.folio','folio')
            .addSelect('boleta')
            .innerJoin(Usuario,'u','boleta.usuario = u.usuarioId')
            .innerJoin(Cliente,'c','c.id = u.cliente')
            .innerJoin(Folio,'f','boleta.folio = f.folioId')
            .where("boleta.fecha >= :fecIni" ,{fecIni})
            .andWhere("boleta.fecha <= :fecFin" ,{fecFin})
            .andWhere("c.id = "+subQuery.cliente)
            .getRawMany();
            res.send(boleta);
        } catch (e) {
            return res.status(404).json({
                             message: e+''
                         })
        }
        
        

    }
    static newBoleta = async (req:Request, res:Response) => {
        const {monto, iva, usuario, folioId} = req.body
        // const folio = await getRepository(Folio)
        //     .createQueryBuilder('folio')
        //     .groupBy("folio.folioId")
        //     .select("MIN(folio.folioId)", "min")
        //     .addSelect("folio.folioId", "folioId")
        //     .where("folio.cliente = :cliente",{cliente})
        //     .andWhere("folio.folioEstado = :folioEstado",{folioEstado:"DISPONIBLE"})
        //     .getRawOne();
        const boleta = new Boleta();
        boleta.monto = monto;
        boleta.iva = iva;
        boleta.usuario = usuario;
        boleta.folio = folioId;

        // Validación de datos

        const validationOpt = {validationError:{target:false, value:false}};
        const errors = await validate(boleta, validationOpt);
        

        if (errors.length > 0) {
            return res.status(400).json(errors);
        }
        const boletaRepository = getRepository(Boleta);
        try {
            await boletaRepository.save(boleta);
        } catch (e) {
            return res.status(409).json({
                message: e+""
            })
        }
        
        const folioRepository = getRepository(Folio);
        const folioEstado = "FIRMADO";
        let folUp;
        try {
            folUp = folioRepository.findOneOrFail(folioId);
            folUp.folioEstado = folioEstado;
            await folioRepository.update(folioId, folUp)
        } catch (e) {
            return res.status(409).json({
                message: e+""
            })
        }
        
        res.status(200).json({
            message: 'Boleta firmada exitosamente'
        })

        

    }

    static editBoleta = async (req:Request, res:Response) => {
        let boleta;
        const {idBoleta} = req.params;
        const {boletaEstado} = req.body;
        const boletaRepository = getRepository(Boleta);

        try {
            boleta = boletaRepository.findOneOrFail(idBoleta);
            boleta.boletaEstado = boletaEstado;
        } catch (e) {
            return res.status(404).json({
                message: 'Boleta no encontrada'
            })
        }
        const validationOpt = {validationError:{target:false, value:false}};
        const errors = await validate(boleta, validationOpt);

        if (errors.length > 0) {
            return res.status(400).json(errors);
        }

        try {
            await boletaRepository.update(idBoleta, boleta)
        } catch (e) {
            return res.status(409).json({
                message: 'No ha sido posible anular la boleta'
            })
        }
        res.status(201).json({
            message: 'La boleta ha sido anulada'
        })
    }
}

export default BoletaController;




