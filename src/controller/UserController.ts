import {getRepository} from "typeorm";
import {Request, Response} from "express";
import {Usuario} from "../entity/User";
import {validate} from 'class-validator';

export class UserController {
    static getAll = async (req: Request, res:Response) => {
        const userRepository = getRepository(Usuario);
        try {
            const users = await userRepository.find();

            if (users.length > 0) {
                res.send(users);
            } else {
                res.status(404).json({
                    message: 'No result'
                })
            }
        } catch (e) {
            return res.status(404).json({
                message: 'Unable to get rows'
            })
        }


    };
    static getById = async (req: Request, res: Response) => {
        const {id} = req.params;
        const userRepository = getRepository(Usuario);
        try {
            const user = await userRepository.findOneOrFail(id);
            res.send(user);
        } catch (e) {
            return res.status(404).json({
                message: 'Not result'
            })
        }
    }
    static newUser = async (req: Request, res: Response) => {
        const {username, nombre, a_paterno, a_materno, email,rut,password,id_cliente} = req.body;
        const user = new Usuario(); 
        user.username = username;
        user.nombre = nombre;
        user.a_paterno = a_paterno;
        user.a_materno = a_materno;
        user.email = email;
        user.rut = rut;
        user.password = password;
        user.cliente = id_cliente;

        // Validate
        const validationOpt = {validationError:{target:false, value:false}};
        const errors = await validate(user, validationOpt);
        if (errors.length > 0) {
            return res.status(400).json(errors);
        }
        // TO DO: HASH PASSWORD 

        const userRepository = getRepository(Usuario);
        try {
            user.hashPassword();
            await userRepository.save(user);
        } catch (e) {
            return res.status(409).json({
                message: e+'!'
            })
        }
        res.status(200).json({
            message: 'User added'
        })
    }
    static editUser = async (req: Request, res: Response) => {
        let user;
        const {id} = req.params;
        const {nombre, a_paterno, a_materno, email} = req.body;

        const userRepository = getRepository(Usuario);
        try {
            user = userRepository.findOneOrFail(id);
            user.nombre = nombre;
            user.a_paterno = a_paterno;
            user.a_materno = a_materno;
            user.email = email;
        } catch (e) {
            return res.status(404).json({
                message: 'User not found'
            })
        }
        const validationOpt = {validationError:{target:false, value:false}};
        const errors = await validate(user, validationOpt);
        
        if (errors.length > 0) {
            return res.status(400).json(errors);
        }
        //Try to save
        try {
            //await userRepository.save(user);
            await userRepository.update(id, user)

        } catch (e) {
            return res.status(409).json({
                message: 'User already in use'
            })
        }
        res.status(201).json({
            message: 'User updated'
        })
    }
    static deleteUser = async (req: Request, res: Response) => {
        const {id} = req.params;
        const userRepository = getRepository(Usuario);
        let user: Usuario;

        try {
            user = await userRepository.findOneOrFail(id);
        } catch (e) {
            return res.status(404).json({
                message: 'User not found'
            })
        }

        // Removing
        userRepository.delete(id);
        res.status(201).json({
            message: 'User deleted'
        })
    };
}

export default UserController;