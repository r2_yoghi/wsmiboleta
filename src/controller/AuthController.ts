import { getRepository } from 'typeorm';
import { Usuario } from './../entity/User';
import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import config from '../config/config';
import UserController from './UserController';


class AuthController {
    static login = async (req: Request, res: Response) => {
        const {username, password} = req.body;

        if (!(username && password)) {
            return res.status(400).json({
                message: 'Username and Password are required!'
            });
        }
        const userReopository = getRepository(Usuario);
        let user: Usuario;
        try {
            user = await userReopository.findOneOrFail({where:{username}})
        } catch (e) {
            return res.status(400).json({
                message: 'Username and Password are incorrect1!'
            })
        }
        // check password
        if (!user.checkPassword(password)) {
            return res.status(400).json({
                message: 'Username and Password are incorrect2!'
            })
        }
        const token = jwt.sign({userid: user.usuarioId, username: user.username}, config.jwtSecret, {expiresIn:'1h'})
        const idusuario = user.usuarioId;
        res.json({
            message: 'OK', token, idusuario 
        });
    };
}
export default AuthController;