import { Folio } from './../entity/Folio';
import { Boleta } from './../entity/Boleta';
import { getRepository, getConnection, IsNull, SelectQueryBuilder } from 'typeorm';
import { Usuario } from './../entity/User';
import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import config from '../config/config';
import {validate} from 'class-validator';
import { isNull } from 'util';

export class FolioController {

    static getFolio = async (req:Request, res: Response) => {
        const {cliente} = req.params;
        const folioEstado = "DISPONIBLE";

        try {
            const folio = await getRepository(Folio)
            .createQueryBuilder('folio')
            .groupBy("folio.folioId")
            .select("MIN(folio.folioId)", "min")
            .addSelect("folio.folio", "folio")
            .where("folio.cliente = :cliente",{cliente})
            .andWhere("folio.folioEstado = :folioEstado",{folioEstado})
            //.getOne();
            .getRawOne() || "";
            if (folio.folio) {
                res.status(200).json(folio)
            } else {
                res.status(409).json({
                    message: 'Folios agotados'
                })
            }
            
            
        } catch (e) {
            return res.status(409).json({
                message: e+""
            })
        }
    };

    static prepFolio = async (req:Request, res: Response) => {
        let folioUp;
        let folioEstado = "DISPONIBLE";
        let folioNewEstado = "PREPARADO";
        const {cliente} = req.params;
        const folioRepository = getRepository(Folio);

        try {
            const subQuery = await getRepository(Folio)
            .createQueryBuilder('folio')
            .select("MIN(folio.folioId)","minfolio")
            .where("folio.cliente = :cliente",{cliente})
            .andWhere("folio.folioEstado = :folioEstado",{folioEstado})
            .getRawOne();
            
            const folio = await getRepository(Folio)
            .createQueryBuilder('folio')
            .select("folio.folioId, folioId")
            .addSelect("folio.folio", "folio")
            .where("folio.folioId = "+subQuery.minfolio)
            .getRawOne() || "";
            
            if (folio.folio) {
                
                folioUp = folioRepository.findOneOrFail(folio.folioId);
                folioUp.folioEstado = folioNewEstado;
                await folioRepository.update(folio.folioId, folioUp)

                res.status(200).json(folio)
            } else {
                res.status(409).json({
                    message: 'Error al preparar folio!'
                })
            }

            
        } catch (e) {
            return res.status(404).json({
                message: 'Folio no encontrado'
            })
        }

    }

    static newFolio = async (req:Request, res:Response) => {
        const {folio, client} = req.body;
        const nfolio = new Folio();
        nfolio.folio = folio;
        nfolio.cliente = client;

        // VALIDATE
        const validationOpt = {validationError:{target:false, value:false}};
        const errors = await validate(nfolio, validationOpt);
        if (errors.length > 0) {
            return res.status(400).json(errors);
        }

        const folioRepository = getRepository(Folio);
        try {
            await folioRepository.save(nfolio);
        } catch (e) {
            return res.status(409).json({
                message: 'El folio '+folio+ ' ya existe',
        })
    
        }
        res.status(200).json({
            message: 'Folio added'
        })
    };
}
export default FolioController;